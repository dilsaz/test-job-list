# Job Management Project

A simple job list project with React

**[click for DEMO](https://dilsaz-job-list.herokuapp.com/)**

**[test report](https://dilsaz-job-list.herokuapp.com/test)**

**[test features (BDD)](https://dilsaz-job-list.herokuapp.com/features)**

## direct app start
- **(install > test > build > start)**
- **default port : 8080**

```bash
yarn app:start
```
---
## Used Technologies
- React.js (Hooks)
- Sass (.scss)
- jest-cucumber (BDD)
- Redux
- Redux-Saga
- Redux DevTools (for dev)
- Immutable.js (for redux)
- Web storage (localStorage with redux)
- Webpack (4)
- Docker
- GitLab CI/CD

**Uikit, which I specially prepared for this project, is used.**

---
## for development
- **first server start**
```bash
yarn start:dev
```

- **for client app**

*open a second terminal.*

- auto starting
    - DevServer
    - Hot Module Replacement (HMR)



```bash
cd ./client
yarn start
```
---
## prod build

```bash
cd ./client
yarn build
cd ../
yarn start
```
---
## client app test
```bash
yarn client:test
```
---
## Docker
*for docker build*
```bash
docker build -t job-list-project .
```
*after start command*
```bash
docker run -it -p 8080:8080 job-list-project
```
*Started at "http://localhost:8080"*

---

## License
[MIT](https://choosealicense.com/licenses/mit/)

