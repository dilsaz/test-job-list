const express       = require('express');
const path          = require('path');
const fs            = require('fs');
const dotenv        = require('dotenv').config();
const featuresPath  = "client/features/scenarios/";
const serverApp     = express();

serverApp.get('/api/priority-types', (_req,res) =>{
    const resultData = {
        status  : 200,
        success : true,
        data    : {
                    "urgent"    : {
                                    name  : "Urgent",
                                    level : 1
                                 },
                    "regular"   : {
                                    name  : "Regular",
                                    level : 2
                                  },
                    "trivial"   : {
                                    name  : "Trivial",
                                    level : 3
                                }
                 }
        };
        
    res.json(resultData);
});

// for client app static files
serverApp.get('/test', (_req, res) => {
    res.sendFile( path.join( __dirname + '/client/test-report/index.html' ) );
});

serverApp.get('/features', (_req, res) => {
    let fileList = [];
    fs.readdirSync( path.join( __dirname, featuresPath ) ).forEach(file => {
        fileList.push(`<li><a href="/features/${file}" target="_blank">${file}</a></li>`);
    });
    res.send(`<ul>${fileList.join('')}</ul>`);
});

serverApp.get('/features/:name', (req, res) => {
    try {
        const file = fs.readFileSync( path.join( __dirname, ( featuresPath +  req.params.name ) ), 'utf8' );
        res.send(`<pre>${file}</pre>`);
    } catch (_error) {
        res.sendStatus(404);
    }
});

serverApp.use( express.static( path.join(__dirname, 'client/dist') ) );

serverApp.get('*', (_req, res) => {
    res.sendFile( path.join( __dirname + '/client/dist/index.html' ) );
});

const serverAppPORT = ( process.env.PORT || 8080 );
const serverAppLine = serverApp.listen(serverAppPORT, () => {
    const params = serverAppLine.address();
    const host   = params.address === "::" ? "localhost" : params.address;
    const port   = params.port;
    console.log("app listening at %s:%s",host,port);
});
module.exports = serverApp;