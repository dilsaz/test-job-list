# Job Management Project - Client App
[project detail](https://gitlab.com/dilsaz/test-job-list)

## development start for client app
- *before server start*
```bash
yarn start
```
---
## prod build

```bash
yarn build
```
---
## test

```bash
yarn test
```
**for test watch**
```bash
yarn test:watch
```
---
## License
[MIT](https://choosealicense.com/licenses/mit/)