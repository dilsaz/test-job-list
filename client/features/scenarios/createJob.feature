Feature: Create new job

Scenario: Empty field validations
    Given the new job fields ready to be used
    When I click the submit button without writing anything
    Then it should not add a new record.
    And the fields "name" and "type" should be error.

Scenario: Creating a new job record.
    Given there is no job record called "test one".
    When I type the name of the job "test one"
    And i selected the type of job "urgent"
    And I click the submit button.
    Then a new job record has been created. its name is "test one" and type is "urgent".