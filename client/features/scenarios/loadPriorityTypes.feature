Feature: Load Priority Types

Scenario: Get priority types
    Given of the application sheet.
    When the application is ready.
    Then a "GET" call is made to the "/api/priority-types" api url address.

Scenario: Set priority types
    Given the storage is ready for use.
    When  "jobData"  in storage exist.
    Then "priorityTypes" key length must be greater than "0" in the "jobData".