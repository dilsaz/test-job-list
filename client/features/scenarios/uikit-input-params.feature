Feature: Uikit-input params

Scenario: English character control
    Given the Uikit "uikit-input" field with "en" language option is ready to use.
    When I type "test data is ŞÇÜĞÇLU"
    Then the value should be "test data is LU".

Scenario: Maximum character control
    Given the data entry field with a maximum limit of "70" characters.
    When i type "80" characters
    Then the value must be "70" characters