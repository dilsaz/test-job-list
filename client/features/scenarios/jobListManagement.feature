Feature: Job List Management

    Background: create three different types of job records
        Given three different job created for testing.

    Scenario: Job search filter.
        Given the list of jobs
        When I type "test job Urgent" in search field.
        Then there should be only "test job Urgent" in the list.

    Scenario: Delete job
        Given the job named "test job Urgent" is in the list.
        When I press the delete button for this job.
        And I confirmed the deletion.
        Then it should be deleted from the list.

    Scenario: Edit job
        Given the job named "test job Regular" and type is "Regular" in the list.
        When I press the edit button for this job.
        And I changed type to "urgent"
        Then it should be seen as type is "Urgent" on the list.