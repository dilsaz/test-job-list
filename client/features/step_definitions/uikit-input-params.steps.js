import { defineFeature, loadFeature }   from 'jest-cucumber';
import Uikit                            from "@uikit";
import { render, fireEvent }            from '@testing-library/react'
import React                            from 'react';
import * as customFunctions             from "@customFunctions";

const feature       = loadFeature('./features/scenarios/uikit-input-params.feature');
let wrapper         = null;

defineFeature(feature, test => {

    test('English character control', ({ given, when, then }) => {
        let fieldClassName = "";
        given(/^the Uikit "(.*)" field with "(.*)" language option is ready to use.$/, (fieldClass,lang) => {
            fieldClassName = fieldClass;
            const Comp     = render( <Uikit.Input lang = {lang} /> );
                  wrapper  = Comp.container;
        });
      
        when(/^I type "(.*)"$/, (arg0) => {
            const field = wrapper.querySelector(`.${fieldClassName} input`);
            expect(field).not.toBe(null);
            fireEvent.change( field, { target: { value : arg0 } } );
        });
      
        then(/^the value should be "(.*)".$/, (arg0) => {
            const field      = wrapper.querySelector(`.${fieldClassName} input`);
            expect(field).not.toBe(null);
            const inputValue = field.value;
            expect(inputValue).toBe(arg0);
        });

      });

      test('Maximum character control', ({ given, when, then }) => {
        given(/^the data entry field with a maximum limit of "(.*)" characters.$/, (arg0) => {
            const maxLength = parseInt(arg0,10);
            const Comp      = render( <Uikit.Input  maxLength={maxLength} /> );
                  wrapper   = Comp.container;
        });
      
        when(/^i type "(.*)" characters$/, (arg0) => {
            const randomValue = customFunctions.randomString( parseInt(arg0, 10) );
            const field       = wrapper.querySelector(".uikit-input input");
            expect(field).not.toBe(null);
            fireEvent.change( field, { target: { value : randomValue } } );
        });
      
        then(/^the value must be "(.*)" characters$/, (arg0) => {
            const field     = wrapper.querySelector(".uikit-input input");
            expect(field).not.toBe(null);
            const maxLength = parseInt(arg0,10);
            expect(field.value.length).toBe(maxLength);
        });

      });

});