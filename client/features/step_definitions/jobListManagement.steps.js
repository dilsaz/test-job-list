import { defineFeature, loadFeature }   from 'jest-cucumber';

import { fireEvent }          from '@testing-library/react'
import createApp              from "@test.app";
import priorityTypes_mockData from "@test.utils.priorityTypes_mockData.json";
import * as actions           from "@actions";

import axios from 'axios';
jest.mock('axios');
axios.mockImplementationOnce( () => Promise.resolve(priorityTypes_mockData) );

const feature   = loadFeature('./features/scenarios/jobListManagement.feature');

let wrapper, renderComp, testApp, store, jobContainer;


defineFeature(feature, test => {

    beforeEach(() => {
        axios.mockImplementationOnce( () => Promise.resolve(priorityTypes_mockData) );
        testApp       = createApp();
        store         = testApp.store;
        renderComp    = testApp.Comp;
        wrapper       = renderComp.container;
        jobContainer  = wrapper.querySelector(".job-list-container");
    });

    afterEach(() => {
        jest.clearAllMocks(); 
        jest.restoreAllMocks();
        axios.mockImplementationOnce( () => Promise.resolve(priorityTypes_mockData) );
    });


    const createTestJobs = (given) => {
        given('three different job created for testing.', () => {
            const state = store.getState();
            const data  = state.get("jobData").toJS();
            const jobs  = data.jobs;
            const typeData = priorityTypes_mockData.data.data;
            if (!jobs.length)
                for( let key in typeData ){
                    store.dispatch( actions.addJob( { name : `test job ${typeData[key].name}`, type : key } ) );
                }
        });
    };

      test('Job search filter.', ({ given, when, then }) => {

        createTestJobs(given); // Background

        given('the list of jobs', () => {
            const jobList = jobContainer.querySelectorAll(".uikit-table .tbody .trow");
            expect(jobList.length).toBe(3);
        });
      
        when(/^I type "(.*)" in search field.$/, (arg0) => {
            const field = jobContainer.querySelector(".job-filter-container .job-name-field input");
            expect(field).not.toBe(null);
            fireEvent.change( field, { target: { value : arg0 } } );
        });
      
        then(/^there should be only (.*) in the list.$/, (arg0) => {
            const jobList   = jobContainer.querySelectorAll(`.uikit-table .tbody .trow`);
            expect(jobList.length).toBe(1);

            const jobRow    = jobList[0].querySelectorAll(`.td [data-value=${arg0}]`);
            expect(jobRow.length).toBe(1);
        });

      });


      test('Delete job', ({ given, when, and, then }) => {

        createTestJobs(given); // Background

        let jobName = "";

        given(/^the job named "(.*)" is in the list.$/, (arg0) => {
                  jobName = arg0;
            const tJob    = jobContainer.querySelectorAll(`.uikit-table .tbody .trow .td [data-value='${jobName}']`);
            expect(tJob.length).toBe(1);
        });
      
        when('I press the delete button for this job.', () => {
            const tJob          = jobContainer.querySelector(`.uikit-table .tbody .trow .td [data-value='${jobName}']`);
            const deleteButton  = tJob.parentElement.parentElement.querySelector(".delete-button");
            expect(deleteButton).not.toBe(null);
            deleteButton.click();
        });
      
        and('I confirmed the deletion.', () => {
           const uiDialog = wrapper.querySelector(".uikit-modal");
           expect(uiDialog).not.toBe(null);

           const confirmButton = uiDialog.querySelector(".done-button-container button");
           expect(confirmButton).not.toBe(null);

           confirmButton.click();
        });
      
        then('it should be deleted from the list.', () => {
            const jobList = jobContainer.querySelectorAll(`.uikit-table .tbody .trow`);
            expect(jobList.length).toBe(2);

            const tJob   = jobContainer.querySelectorAll(`.uikit-table .tbody .trow .td [data-value='${jobName}']`);
            expect(tJob.length).toBe(0);

        });

      });


      test('Edit job', ({ given, when, and, then }) => {

        createTestJobs(given); // Background

        let jobID = "";

        given(/^the job named "(.*)" and type is "(.*)" in the list.$/, (name, _type) => {
            const tJob    = jobContainer.querySelector(`.uikit-table .tbody .trow .td [data-value='${name}']`);
            expect(tJob).not.toBe(null);
            jobID = tJob.parentElement.parentElement.dataset.id;
        });
      
        when('I press the edit button for this job.', () => {
            const tJob          = jobContainer.querySelector(`.uikit-table .tbody .trow[data-id='${jobID}']`);
            const deleteButton  = tJob.querySelector(".edit-button");
            expect(deleteButton).not.toBe(null);
            deleteButton.click();
        });
      
        and(/^I changed type to "(.*)"$/, (newType) => {
            const uiModal = wrapper.querySelector(".uikit-modal");
            expect(uiModal).not.toBe(null);
            
            const field = uiModal.querySelector(".type-content select");
            expect(field).not.toBe(null);
            fireEvent.change( field, { target: { value : newType } } );

            const confirmButton = uiModal.querySelector(".done-button-container button");
            expect(uiModal).not.toBe(null);
            confirmButton.click();
        });
      
        then(/^it should be seen as type is "(.*)" on the list.$/, (newType) => {
            const tJob              = jobContainer.querySelector(`.uikit-table .tbody .trow[data-id='${jobID}']`);
            const jobTypeSelector   = tJob.querySelectorAll(`[data-value='${newType}']`);
            expect(jobTypeSelector.length).toBe(1);
        });
        
      });
 
});