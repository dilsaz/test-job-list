import { defineFeature, loadFeature }   from 'jest-cucumber';

import { fireEvent }          from '@testing-library/react'
import createApp              from "@test.app";
import priorityTypes_mockData from "@test.utils.priorityTypes_mockData.json";

import axios from 'axios';
jest.mock('axios');

const feature   = loadFeature('./features/scenarios/createJob.feature');

let wrapper, renderComp, testApp, store, jobCreateForm;

axios.mockImplementationOnce( () => Promise.resolve(priorityTypes_mockData) );

defineFeature(feature, test => {

    beforeEach(() => {
        axios.mockImplementationOnce( () => Promise.resolve(priorityTypes_mockData) );
        testApp       = createApp();
        store         = testApp.store;
        renderComp    = testApp.Comp;
        wrapper       = renderComp.container;
        jobCreateForm = wrapper.querySelector("#job-create-form");
    });

    afterEach(() => {
        jest.clearAllMocks(); 
        jest.restoreAllMocks();
        axios.mockImplementationOnce( () => Promise.resolve(priorityTypes_mockData) );
    });


    test('Empty field validations', ({ given, when, then, and }) => {

        given('the new job fields ready to be used', () => {
            const createJobObjects  = jobCreateForm.querySelectorAll(".job-name-field, .job-type-field, [type=submit]");
            expect(createJobObjects.length).toBe(3);
        });
      
        when('I click the submit button without writing anything', () => {
            const submitButton  = jobCreateForm.querySelector("[type=submit]");
            submitButton.click();
        });
      
        then('it should not add a new record.', () => {
            const state = store.getState();
            const data  = state.get("jobData").toJS();
            const jobs  = data.jobs;
            expect(jobs.length).toBe(0);
        });

        and(/^the fields "(.*)" and "(.*)" should be error.$/, (name,type) => {
            const errorFields   = jobCreateForm.querySelectorAll(`.job-${name}-field.error, .job-${type}-field.error`);
            expect(errorFields.length).toBe(2);
        });

    });

    test('Creating a new job record.', ({ given, when, and, then }) => {


        given(/^there is no job record called "(.*)".$/, (arg0) => {
            const state = store.getState();
            const data  = state.get("jobData").toJS();
            const jobs  = data.jobs;
            const tJob  = jobs.filter( t => t.name === arg0 );
            expect(tJob.length).toBe(0);
        });
      
        when(/^I type the name of the job "(.*)"$/, (arg0) => {
            const field = jobCreateForm.querySelector(".job-name-field input");
            expect(field).not.toBe(null);
            fireEvent.change( field, { target: { value : arg0 } } );
        });
      
        and(/^i selected the type of job "(.*)"$/, (arg0) => {
            const field = jobCreateForm.querySelector(".job-type-field select");
            expect(field).not.toBe(null);
            fireEvent.change( field, { target: { value : arg0 } } );
        });
      
        and('I click the submit button.', () => {
            const submitButton  = jobCreateForm.querySelector("[type=submit]");
            expect(submitButton).not.toBe(null);
            submitButton.click();
        });

        then(/^a new job record has been created. its name is "(.*)" and type is "(.*)".$/, (arg0,arg1) => {
            const state = store.getState();
            const data  = state.get("jobData").toJS();
            const jobs  = data.jobs;
            const tJob  = jobs.filter( t => t.name === arg0 && t.type === arg1 );
            expect(tJob.length).toBe(1);
        });

      });
 
});