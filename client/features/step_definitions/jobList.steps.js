import { defineFeature, loadFeature }   from 'jest-cucumber';

import createApp              from "@test.app";
import priorityTypes_mockData from "@test.utils.priorityTypes_mockData.json";
import * as actions           from "@actions";

import axios from 'axios';
jest.mock('axios');

const feature   = loadFeature('./features/scenarios/jobList.feature');

let wrapper, renderComp, testApp, store, jobContainer;

defineFeature(feature, test => {

    beforeEach(() => {
        axios.mockImplementationOnce( () => Promise.resolve(priorityTypes_mockData) );
        testApp       = createApp();
        store         = testApp.store;
        renderComp    = testApp.Comp;
        wrapper       = renderComp.container;
        jobContainer  = wrapper.querySelector(".job-list-container");
    });

    afterEach(() => {
        jest.clearAllMocks(); 
        jest.restoreAllMocks();
    });


    const createTestJobs = (given) => {
        given('three different job created for testing.', () => {
            const state = store.getState();
            const data  = state.get("jobData").toJS();
            const jobs  = data.jobs;
            const typeData = priorityTypes_mockData.data.data;
            if (!jobs.length)
                for( let key in typeData ){
                    store.dispatch( actions.addJob( { name : `test job ${typeData[key].name}`, type : key } ) );
                }
        });
    };


    test('Sorting the jobs according to priorities.', ({ given, when, then }) => {

        createTestJobs(given); // Background

        given('the test jobs.', () => {
            const state = store.getState();
            const data  = state.get("jobData").toJS();
            const jobs  = data.jobs;
            expect(jobs.length).toBe(3);
        });
      
        when('three records are listed.', () => {
            const jobList = jobContainer.querySelectorAll(".uikit-table .tbody .trow");
            expect(jobList.length).toBe(3);
        });
      
        then('it should be listed according to priorities.', () => {
            let jobCheckResponse    = [];
            let prioritysList       = [];
            const typeData          = priorityTypes_mockData.data.data;

            for( let key in typeData ){
                prioritysList.push({ name : typeData[key].name, level : typeData[key].level });
            }

            prioritysList = prioritysList.sort( (a, b ) => a.level - b.level );

            prioritysList.map( (t,i) => {
                const tPriority = `.uikit-table .tbody .trow:nth-child(${(i+1)}) .td [data-value="${t.name}"] `;
                const job = jobContainer.querySelectorAll(tPriority);
                jobCheckResponse.push( job.length === 1 );
            });
            expect( jobCheckResponse.toString() ).toBe( [ true, true, true ].toString() );
        });
      });
});