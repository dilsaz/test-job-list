import { defineFeature, loadFeature }   from 'jest-cucumber';

import createApp              from "@test.app";
import priorityTypes_mockData from "@test.utils.priorityTypes_mockData.json";

import axios from 'axios';
jest.mock('axios');

const feature   = loadFeature('./features/scenarios/loadPriorityTypes.feature');

axios.mockImplementationOnce( () => Promise.resolve(priorityTypes_mockData) );
const testApp    = createApp();
const store      = testApp.store;
const renderComp = testApp.Comp;
const wrapper    = renderComp.container;

defineFeature(feature, test => {

    test('Get priority types', ({ given, when, then }) => {

      given('of the application sheet.', () => {
        expect(wrapper).not.toBe(null);
      });
  
      when('the application is ready.', () => {
        const mainDocument = wrapper.querySelectorAll("main");
        expect(mainDocument.length).toBe(1);
      });
  
      then(/^a "(.*)" call is made to the "(.*)" api url address.$/, (method,url) => {
        expect(axios).toHaveBeenCalledWith( { "url": url, method : method } );
      });

    });

    test('Set priority types', ({ given, when, then }) => {

      given('the storage is ready for use.', () => {
        const state = store.getState();
        expect(state).not.toBe(null);
      });
    
      when(/^"(.*)"  in storage exist.$/, (arg0) => {
        const state = store.getState();
        const data = state.get(arg0);
        expect(data).not.toBe(null);
      });
    
      then(/^"(.*)" key length must be greater than "(.*)" in the "(.*)".$/, (arg0,arg1,arg2) => {
        const state           = store.getState();
        const data            = state.get(arg2).toJS();
        const keyLength       = Object.keys( data[arg0] ).length;
        const referenceCheck  = keyLength > parseInt(arg1,10);
        expect(referenceCheck).toBe(true);
      });

  
    });

});