import React, { useEffect }   from 'react';
import { useDispatch }        from 'react-redux';
import Layout                 from "@layout";
import Job                    from "@job";
import * as actions           from "@actions";
import Uikit                  from "@uikit";

 const App = () => {

  const dispatch  = useDispatch();

  useEffect(() => {
    dispatch( actions.loadPriorityTypes() );
  }, []);

  return (
      <React.Fragment>
        <Layout>
          <Job.Create />
          <Job.List />
        </Layout>
        <Uikit.Modal />
      </React.Fragment>
  );

}

export default App;