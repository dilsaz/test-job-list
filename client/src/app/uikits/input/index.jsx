import React, 
{ useState, useRef, useEffect } from 'react';
import PropTypes                from 'prop-types';
import textLangController       from "@textLangController";
import Uikit                    from "@uikit";
import { iconList }             from "@uiVariables";

const usePrevious = (value) => {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
}

const UikitInput = (props) => {
  const [value, setValue] = useState(props.defaultValue);
  const prevDefaultValue  = usePrevious(props.defaultValue);

  if ( 
      props.defaultValue !== prevDefaultValue 
      && ( props.defaultValue || prevDefaultValue )
      && props.defaultValue !== value
    )
    setValue(props.defaultValue);


  const onChangeHandler = (e) => {
    let eValue = e.target.value || "";
        eValue = textLangController(eValue,props.lang);
        eValue = eValue.substring(0, props.maxLength);

    setValue(eValue);
    props.onChange(eValue);
  }

  const classList = () => {
    let classNames = [`uikit-input`];
    
    if (props.className)  classNames.push(props.className);
    if (props.fullWidth)  classNames.push("full");
    if (props.isError)    classNames.push("error");
 
    if (props.className)
      Array.prototype.push.apply( classNames, props.className.split(' ') );

    return [...new Set(classNames)].join(' ');
  }

  return (
        <label className={ classList() }>
          <span className="title">{ props.label }</span>
          { props.icon ? <Uikit.Icon name={props.icon} className="prefix-icon"/> : null }
          <input 
                value       = { value } 
                onChange    = { onChangeHandler } 
                required    = { props.required }
                maxLength   = { props.maxLength }
                placeholder = { props.placeholder }
                type        = { props.type }
                disabled    = { props.disabled }
          />
        </label>
  );

}

UikitInput.propTypes = {
    type         : PropTypes.oneOf(['email', 'number', 'password', 'search', 'tel', 'text', 'url']),
    className    : PropTypes.string,
    label        : PropTypes.string,
    maxLength    : PropTypes.number,
    required     : PropTypes.bool,
    placeholder  : PropTypes.string,
    lang         : PropTypes.oneOf(['auto','en','tr']),
    defaultValue : PropTypes.string,
    onChange     : PropTypes.func,
    fullWidth    : PropTypes.bool,
    disabled     : PropTypes.bool,
    icon         : PropTypes.oneOf(iconList),
    isError      : PropTypes.bool
}

UikitInput.defaultProps = {
    type          : 'text',
    lang          : "auto",
    maxLength     : 255,
    required      : false,
    disabled      : false,
    isError       : false,
    defaultValue  : "",
    onChange      : () => {},
    fullWidth     : false
};

export default UikitInput;