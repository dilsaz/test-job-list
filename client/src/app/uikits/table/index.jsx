import React    from "react";
import Uikit    from "@uikit";

const getWidth = (headData,i) => {
    return ( headData[i] || {} ).width || "auto";
}

const getHead  = ( { head } ) => {
    let result = head.map( (t,i) =>  
                                <div 
                                    className   = "th" 
                                    key         = {`th-${i}`} 
                                    style       = { { width : t.width || "auto" } }
                                >{ t.value }</div>
                 );
    if (result)
        return <div className="thead">
                    <div className="trow">
                        { result }
                    </div>
                </div>
}

const noContent = () => {
    return <div className="table-no-body">
                <Uikit.Icon  name="exclamation" />
                <span className="title">No Record Found</span>
            </div>
};

const getBody  = ( { body, head } ) => {

    if (!body.length)
       return noContent();

    const customClass = body.length < 8 ? "min-bdoy" : "";
    let result = body.map( (trow,i) => {

                    const rowData = trow.cels.map( (t,i2) => 
                                        <div 
                                            className   = "td" 
                                            key         = {`td-${i2}`} 
                                            style       = { { width : getWidth(head,i2) } }
                                        >{ t }</div>
                                    );

                    return  <div className="trow" key={`tr-${i}`} data-id={trow.id} >{ rowData }</div>
                });
    if (result)
        return <div className= {`tbody ${customClass}`}>{result}</div>
};

const UikitTable = (props) => {

    return (
        <div className="uikit-table">
           { getHead(props.data) }
           { getBody(props.data) }
        </div>
    );
};
export default UikitTable;