import React            from "react";
import PropTypes        from 'prop-types';
import { iconList }     from "@uiVariables";

const UikitIcon = (props) => {
    return (
        <i className={`uikit-icon uikit-icon-${props.name} ${props.className}`}></i>
    );
};

UikitIcon.propTypes = {
    name         : PropTypes.oneOf(iconList).isRequired,
    className    : PropTypes.string
}

UikitIcon.defaultProps = {
    color         : 'primary',
    className     : ""
};

export default UikitIcon;