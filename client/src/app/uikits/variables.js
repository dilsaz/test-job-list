export const iconList = [
    'edit',
    'plus',
    'check',
    'trash',
    'down-arrow',
    'up-arrow',
    'cancel',
    'loupe',
    'filter',
    'exclamation'
];

export const colorList = [
    '',
    'primary',
    'secondary',
    'success',
    'danger',
    'warning',
    'info',
    'light'
];