import React, 
{ useState, useRef, useEffect } from 'react';
import PropTypes                from 'prop-types';

const usePrevious = (value) => {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
}

const UikitSelectBox = (props) => {

    const [value, setValue] = useState(props.defaultValue);
    const prevDefaultValue  = usePrevious(props.defaultValue);
    
    if ( 
        props.defaultValue !== prevDefaultValue 
        && ( props.defaultValue || prevDefaultValue )
        && props.defaultValue !== value
      )
      setValue(props.defaultValue);

    const onChangeHandler = (e) => {
      let eValue = e.target.value || "";
      setValue(eValue);
      props.onChange(eValue);
    }

    const classList = () => {
      let classNames = [`uikit-select-box`];
      
      if (props.className)  classNames.push(props.className);
      if (props.fullWidth)  classNames.push("full");
      if (props.isError)    classNames.push("error");
    
      if (props.className)
        Array.prototype.push.apply( classNames, props.className.split(' ') );
  
      return [...new Set(classNames)].join(' ');
    }

    const getOptions = () => {
      return props.data && 
             props.data.map( t => <option key={t.value} value={ t.value }>{ t.text }</option> );
    }

    const getPlaceholder = () => {
      const disabledProps = props.required ? { disabled: "disabled" } : null;
      if ( props.placeholder !== null )
        return <option value="" { ...disabledProps }>{ props.placeholder }</option>
    }

    return (
            <label className={ classList() }>
                <span className="title">{ props.label }</span>
                <select value={ value } onChange={ onChangeHandler } required={props.required} >
                  { getPlaceholder() }
                  { getOptions() }
                </select>
            </label>
    );
};

UikitSelectBox.propTypes = {
  className    : PropTypes.string,
  label        : PropTypes.string,
  required     : PropTypes.bool,
  placeholder  : PropTypes.string,
  defaultValue : PropTypes.string,
  onChange     : PropTypes.func,
  fullWidth    : PropTypes.bool,
  isError      : PropTypes.bool,
  data         : PropTypes.arrayOf(
                  PropTypes.exact({
                    value   : PropTypes.oneOfType([
                              PropTypes.string,
                              PropTypes.number
                            ]),
                    text : PropTypes.oneOfType([
                              PropTypes.string,
                              PropTypes.number
                            ]),
                  })
                )
};

UikitSelectBox.defaultProps = {
  required      : false,
  isError       : false,
  defaultValue  : "",
  onChange      : () => {},
  fullWidth     : false,
  placeholder   : "Choose"
};

export default UikitSelectBox;