import UikitInput       from "@uikit.input";
import UikitButton      from "@uikit.button";
import UikitIcon        from "@uikit.icon";
import UikitSelectBox   from "@uikit.selectbox";
import UikitTable       from "@uikit.table";
import UikitBadge       from "@uikit.badge";
import UikitModal       from "@uikit.modal";

export default {
    Input       : UikitInput,
    Button      : UikitButton,
    Icon        : UikitIcon,
    SelectBox   : UikitSelectBox,
    Table       : UikitTable,
    Badge       : UikitBadge,
    Modal       : UikitModal
};