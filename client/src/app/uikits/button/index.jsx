import React            from "react";
import PropTypes        from 'prop-types';
import Uikit            from "@uikit";

import {iconList, colorList }       from "@uiVariables";

const UikitButton = (props) => {

    const classList = () => {
        let classNames = [`uikit-button btn-${props.color}`];
        
        if (props.color)      classNames.push(`btn-${props.color}`);
        if (props.className)  classNames.push(props.className);
        if (props.size)       classNames.push(`size-${props.size}`);
        if (props.border)     classNames.push("border");
        if (props.fullWidth)  classNames.push("full");
     
        if (props.className)
          Array.prototype.push.apply( classNames, props.className.split(' ') );

        return [...new Set(classNames)].join(' ');
      }

    return (
        <button className={ classList() }  type={ props.type } onClick={ props.onClick } >
            { props.icon    ? <Uikit.Icon name={props.icon} className={ props.value ? "m-r-5" : "" } />  : null }
            { props.value   ? <span>{ props.value }</span>      : null }
        </button>
    );
};

UikitButton.propTypes = {
    type         : PropTypes.oneOf([ 'reset', 'submit', 'button' ]),
    size         : PropTypes.oneOf([ 'default', 'small' ]),
    value        : PropTypes.string,
    className    : PropTypes.string,
    icon         : PropTypes.oneOf(iconList),
    iconColor    : PropTypes.oneOf(colorList),
    color        : PropTypes.oneOf(colorList),
    border       : PropTypes.bool,
    fullWidth    : PropTypes.bool,
    onClick      : PropTypes.func
}

UikitButton.defaultProps = {
    type          : 'button',
    size          : 'default',
    value         : '',
    color         : 'primary',
    className     : "",
    onClick       : () => {}
};

export default UikitButton;