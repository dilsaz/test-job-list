import React, { useState }          from "react";
import { useSelector, useDispatch } from 'react-redux';
import Uikit                        from "@uikit";
import * as actions                 from "@actions";

const stateSelector = (state) => ({
    globalData   : state.get('globalData').toJS(),
});

const UikitModal = () => {
    const dispatch                          = useDispatch()
    const [status, setStatus]               = useState(false);
    const [renderStatus, setRenderStatus]   = useState(false);
    const stateData                         = useSelector(stateSelector);
    const modalData                         = stateData.globalData.modal;
    const modalStatusClass                  = status ? "active" : "";

    if ( modalData.status && !renderStatus && !status ){
        setRenderStatus(true);
        setTimeout(() => {
            setStatus(true);
        }, 100);
    }

    if ( !modalData.status && renderStatus && status ){
        setStatus(false);
        setTimeout(() => {
            setRenderStatus(false);
        }, 200);
    }

    const onCloseHandler = (closeStatus) => {
        dispatch( actions.closeModal() );
        if ( typeof modalData.onClose === "function" )
            modalData.onClose(closeStatus);
    };

    if (!renderStatus)
        return <React.Fragment></React.Fragment>

    return (
        <div className={`uikit-modal ${modalStatusClass}`}>
            <div className="modal-container">
                
                {
                    modalData.icon ?
                        <div className="icon">
                            <Uikit.Icon name={ modalData.icon } color="danger" />
                        </div>
                    : null
                }
                
                <div className="title">{ modalData.title || "" }</div>

                { modalData.body ? <div className="body">{ modalData.body }</div> : null }

                {
                    modalData.isDialog ? 
                        <div className="footer">
                            <div className="cancel-button-container">
                                <Uikit.Button 
                                    onClick   = { () => onCloseHandler(false) }
                                    value     = "Cancel" 
                                    fullWidth = { true } 
                                    color     = "light" 
                                />
                            </div>
                            <div className="done-button-container">
                                <Uikit.Button 
                                    value     = "Approve" 
                                    fullWidth = { true } 
                                    color     = "danger" 
                                    onClick   = { () => onCloseHandler(true) }
                                />
                            </div>
                        </div>
                    : null
                }
              

            </div>
        </div>
    );
};

export default UikitModal;