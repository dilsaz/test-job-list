import React from "react";
const UikitBadge = (props) => {
    return (
        <div className={ `uikit-badge badge-${props.color}`} data-value={props.value} >
            { props.value }
        </div>
    );
};
export default UikitBadge;