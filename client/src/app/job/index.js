import JobCreate    from "@job.create";
import JobList      from "@job.list";
import JobEdit      from "@job.edit";

export default {
    Create : JobCreate,
    List   : JobList,
    Edit   : JobEdit
};