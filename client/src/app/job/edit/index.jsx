import React, { useState }          from "react";
import PropTypes                    from 'prop-types';
import Uikit                        from "@uikit";
import { useSelector, useDispatch } from 'react-redux';
import * as actions                 from "@actions";
import selectboxDataConvert         from "@selectboxDataConvert";

const stateSelector = (state) => ({
    jobData   : state.get('jobData').toJS(),
});

const JobEdit = (props) => {
    const dispatch              = useDispatch();
    const stateData             = useSelector(stateSelector);
    const jobData               = stateData.jobData;
    const jobList               = jobData.jobs || [];
    const priorityTypes         = selectboxDataConvert(jobData.priorityTypes);
    const job                   = jobList[props.jobIndex] || {};
    const [jobType, setJobType] = useState(job.type);

    const editJobHandler = () => {
        const newJobData = {
            index : props.jobIndex,
            data  : {
                ...job,
                type : jobType
            }
        };
        dispatch( actions.editJob(newJobData) );
        dispatch( actions.closeModal() );
    }
    
    return (
        <div className="job-edit-container">
            <div className="name-content m-b-15 text-left">
                <Uikit.Input
                    label        = "Job Name"
                    fullWidth    = { true }
                    defaultValue = { job.name }
                    disabled     = { true }
                />
            </div>
            <div className="type-content m-b-15 text-left">
                <Uikit.SelectBox
                    onChange     = { v => setJobType(v) }
                    label        = "Job Priority"
                    required     = { true }
                    fullWidth    = { true }
                    data         = { priorityTypes }
                    defaultValue = { jobType }
                />
            </div>
            <div className="footer">
                <div className="cancel-button-container">
                    <Uikit.Button 
                        onClick   = { () => dispatch( actions.closeModal() ) }
                        value     = "Cancel" 
                        fullWidth = { true } 
                        color     = "light" 
                    />
                </div>
                <div className="done-button-container">
                    <Uikit.Button 
                        value     = "Save" 
                        fullWidth = { true } 
                        color     = "danger" 
                        onClick   = { editJobHandler }
                    />
                </div>
            </div>
        </div>
    );
};

JobEdit.propTypes = {
    jobIndex    : PropTypes.number.isRequired,
};

export default JobEdit;