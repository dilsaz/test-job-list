import React, { useState }          from "react";
import Uikit                        from "@uikit";
import { useSelector, useDispatch } from 'react-redux';
import * as actions                 from "@actions";
import selectboxDataConvert         from "@selectboxDataConvert";

const stateSelector = (state) => ({
    jobData   : state.get('jobData').toJS(),
});

const JobCreate = () => {

    const [job, setJob]         = useState( { name : "", type : "" } );
    const [isError, setError]   = useState( { name : false, type : false } );
    const dispatch              = useDispatch();
    const stateData             = useSelector(stateSelector);
    const priorityTypes         = selectboxDataConvert(stateData.jobData.priorityTypes);

    const formSubmitHandler = (e) => {
        e.preventDefault();
        // browser validation has been activated. cannot be empty.
        if (job.name && job.type){
            dispatch( actions.addJob(job) );
            setJob({ name : "", type : "" });
        }else{
            setError({ name : !job.name, type : !job.type });
            //window.alert("please fill in all fields.");
        }
    }

    return (
        <form id="job-create-form" onSubmit={formSubmitHandler}>
            <label className="title">Create New Job</label>
            <div className="value-fields">
                    <div className="field-col job-name-field-container" >
                        <Uikit.Input
                            onChange     = { (v) => setJob({ ...job, name : v }) }
                            lang         = "en"
                            label        = "Job Name"
                            maxLength    =  { 70 }
                            required     = { true }
                            fullWidth    = { true }
                            defaultValue = { job.name }
                            isError      = { isError.name }
                            className    = "job-name-field"
                        />
                    </div>
                    <div className="field-col job-type-field-container">
                        <Uikit.SelectBox
                            onChange     = { (v) => setJob({ ...job, type : v } ) }
                            label        = "Job Priority"
                            required     = { true }
                            fullWidth    = { true }
                            data         = { priorityTypes }
                            defaultValue = { job.type }
                            isError      = { isError.type }
                            className    = "job-type-field"
                        />
                    </div>
                    <div className="submit-container">
                        <Uikit.Button 
                            type        = "submit" 
                            value       = "Create" 
                            fullWidth   = { true }
                            icon        = "plus"
                        />
                    </div>
            </div>
        </form>
    );
};

export default JobCreate;