export default (jobListData, prioritys, filter) => {
    let resultData  = setIndexNumber(jobListData);
        resultData  = filterData(filter, resultData);
        resultData  = shortData(resultData, prioritys)
    return resultData;
};

// The index number must be known for the data does not have an ID.
const setIndexNumber = (jobs) => {
    return jobs.map( (t,i) => ({ ...t, index : i }) );
};

const filterData = (filter,jobs) => {
    if (!filter.name && !filter.type)
        return jobs;

    const filterName = ( filter.name || "" ).toLowerCase();
    const filterType = ( filter.type || "" ).toLowerCase();

    return jobs.filter( t => {
        const nameStatus = filterName ? t.name.toLowerCase().includes(filterName) : true;
        const typeStatus = filterType ? t.type === filterType : true;
        return nameStatus && typeStatus;
    });
};

const shortData = (jobs, prioritys) => {
    return jobs.sort( (a, b ) => ( (prioritys[a.type] || {}).level || 99 ) - ( (prioritys[b.type] || {}).level || 99 ) );
};