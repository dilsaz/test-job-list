import React        from "react"
import Uikit        from "@uikit";
import JobEdit      from "@job.edit";
import * as actions from "@actions";
let dispatch        = () => {};

export default (jobs, prioritys, pDispatch) => {
    dispatch = pDispatch;
    return tableData(jobs, prioritys);
};

const tableData = (jobs, prioritys) => ({
    head : [
        {
            value : "Name",
            width : "65%"
        },
        {
            value : "Priority",
            width : "22%"
        },
        {
            value : "Action",
            width : "13%"
        }
    ],
    body : jobDataToTableBody(jobs,prioritys)
});

const jobDataToTableBody = (jobs,prioritys) => {
    return jobs.map( (t,i) => ({ id    : t.index || i,
                                 cels  : [
                                    <span data-value={t.name}>{t.name}</span>,
                                    priorityValue(t.type,prioritys),
                                    <div className="actions"> { editButton(t.index || i) } { deleteButton(t.index || i) } </div>
                                 ]
                              })

            );
};

const priorityValue = (type, prioritys) =>{
    const colorTypes = {
        "regular" : "warning",
        "trivial" : "primary",
        "urgent"  : "danger"
    };
    return <Uikit.Badge value={ ( (prioritys[type] || {}).name || type ) } color={ colorTypes[type] } />
};

const editButton = (i) => {
    
    const openModal = () => {
        const modalData = { 
                    title   : "Job Edit",
                    body    : <JobEdit jobIndex={i} />
                };
        dispatch( actions.openModal(modalData) );
    };

    return <Uikit.Button icon="edit" color="light" className="edit-button" onClick={openModal} />
};

const deleteButton = (i) => {

    const openDialog = () => {
        const dialogData = { 
                    icon    : "exclamation", 
                    title   : "Are you sure you want to delete it?",
                    onClose : r => dialogOnClose(r,i)
                };
        dispatch( actions.openDialog(dialogData) );
    };

    return <Uikit.Button icon="trash" color="light" className="delete-button" onClick={openDialog} />
};

const dialogOnClose = (r,i) => {
    if (r) dispatch(actions.removeJob(i));
};