import React, { useState }          from "react"
import Uikit                        from "@uikit";
import { useSelector, useDispatch } from 'react-redux';
import JobFilter                    from "@job.list.filter";
import handleDataList               from "./utils/handleDataList";
import tableData                    from "./utils/tableData";

const stateSelector = (state) => ({
    jobData   : state.get('jobData').toJS(),
});

const JobList = () => {
    const [filter, setFilter]   = useState({});
    const stateData             = useSelector(stateSelector);
    const jobData               = stateData.jobData;
    const prioritys             = jobData.priorityTypes;
    const jobListData           = jobData.jobs;
    const jobs                  = handleDataList(jobListData, prioritys, filter);
    const lengthText            = `(${ jobs.length }/${ jobListData.length })`;
    const dispatch              = useDispatch();

    return (
        <div className="job-list-container">
            <div className="title-container">
                <label className="title">Job List</label>
                <small className="count">{ lengthText }</small> 
            </div>
            <JobFilter onChange={ d  => setFilter( { ...filter, ...d } ) } />
            <Uikit.Table data={ tableData(jobs, prioritys, dispatch) } />
        </div>
    );
};
export default JobList;