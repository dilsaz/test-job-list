import React                from "react";
import { useSelector }      from 'react-redux';
import Uikit                from "@uikit";
import selectboxDataConvert from "@selectboxDataConvert";

const stateSelector = (state) => ({
    jobData   : state.get('jobData').toJS(),
});

const JobFilter = (props) => {
    const stateData         = useSelector(stateSelector);
    const priorityTypes     = selectboxDataConvert(stateData.jobData.priorityTypes);

    return (
        <div className="job-filter-container">
            <Uikit.Input
                    onChange    = { v => props.onChange( { name : v } ) }
                    className   = "job-name-field"
                    lang        = "en"
                    placeholder = "Job Name"
                    maxLength   =  { 70 }
                    required    = { true }
                    fullWidth   = { true }
                    type        = "search"
                    icon        = "loupe"
            />
            <Uikit.SelectBox
                    onChange    = { v => props.onChange( { type : v } ) }
                    className   = "job-priority-field"
                    placeholder = "Priority (all)"
                    fullWidth   = { true }
                    data        = { priorityTypes }
            />
        </div>
    );
};

export default JobFilter;