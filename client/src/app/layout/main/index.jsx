import React        from "react";
import PropTypes    from 'prop-types';

const Main = (props) => {
    return (
        <main>
            <div className="content">
                { props.children }
            </div>
        </main>
    );
};

Main.propTypes = {
    children    : PropTypes.oneOfType([
                    PropTypes.arrayOf(PropTypes.node),
                    PropTypes.node
                ])
};

export default Main;