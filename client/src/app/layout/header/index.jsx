import React    from "react";
import logo     from "@image.logo.svg";

const Header = () => {
    return (
        <header>
            <div className="content">
                <a className="logo-container" href="/">
                    <img src={logo} alt="logo" className="logo"/>
                    <span className="title">LOGO</span>
                </a>
            </div>
        </header>
    );
}

export default Header;