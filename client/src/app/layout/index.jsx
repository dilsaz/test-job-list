import React        from "react";
import PropTypes    from 'prop-types';
import Header       from "@layout.header";
import Footer       from "@layout.footer";
import Main         from "@layout.main";

const Layout = (props) => {
    return (
        <React.Fragment>
            <Header/>
            <Main>{ props.children }</Main>
            <Footer/>
        </React.Fragment>
    );
};

Layout.propTypes = {
    children    : PropTypes.oneOfType([
                    PropTypes.arrayOf(PropTypes.node),
                    PropTypes.node
                ])
};

export default Layout;