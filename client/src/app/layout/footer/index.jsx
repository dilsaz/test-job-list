import React from "react";
import gitIcon from "@image.git.svg";
const Footer = () => {
    return (
        <footer>
            <div className="content">
                <a href="https://gitlab.com/dilsaz/test-job-list" target="_blank">
                    <img src={gitIcon} alt="git" className="git"/>
                    repository
                </a>
                <span>© 2021 Dilsaz Oktayı</span>
            </div>
        </footer>
    );
};
export default Footer;