import axios from 'axios';

axios.defaults.validateStatus = (status) => {
    return status >= 200 && status < 400;
};

class API{
    constructor(){
        this.name = 'API';
    }
    fetch(param){
        let { url, method, data } = param;
        return new Promise( (resolve, reject) => {
            axios({ 
                method  : method, 
                url     : url,
                data    : data
            }).then( response => {
                if (response.data.success){
                    resolve(response.data);
                }else{
                    reject(response.data);
                }
            }).catch( err => {
                reject( err );
            });
        });
    }
}
export default new API();