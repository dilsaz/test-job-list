export default (data={}) => {
    let result = [];
    for( let key in data ){
        result.push( { text : data[key].name, value : key } );
    }
    return result;
}