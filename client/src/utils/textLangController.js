export default ( value = "", lang = "auto" ) => {

    const langRegx = {
        "en"    : /[^\w\s]/gi,
        "tr"    : /[^\w\söÖçÇİiŞşÜüĞğ]/gi
    };

    if ( !langRegx[lang] )
        return value;

    return value.replace( langRegx[lang], "" );
};