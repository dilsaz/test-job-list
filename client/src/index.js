import React          from 'react';
import ReactDOM       from 'react-dom';
import { Provider }   from 'react-redux';
import App            from '@app';
import appStoreData   from "@appStoreData";
import '@styles';

const store = appStoreData.start();

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root'),
);

if (module.hot)
  module.hot.accept();