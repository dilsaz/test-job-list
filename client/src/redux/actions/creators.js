import * as actionTypes     from "@actionTypes";

export const updatePriorityTypes = (payload) => ({
    type: actionTypes.UPDATE_PRIORITY_TYPES,
    payload: payload
});

export const loadPriorityTypes = () => ({
    type: actionTypes.LOAD_PRIORITY_TYPES
});

export const addJob = (payload) => ({
    type: actionTypes.ADD_JOB,
    payload: payload
});

export const editJob = (payload) => ({
    type: actionTypes.EDIT_JOB,
    payload: payload
});

export const removeJob = (payload) => ({
    type: actionTypes.REMOVE_JOB,
    payload: payload
});

export const openModal = (payload) => ({
    type: actionTypes.OPEN_MODAL,
    payload: {
        ...payload,
        status   : true,
        isDialog : false
    }
});

export const openDialog = (payload) => ({
    type: actionTypes.OPEN_MODAL,
    payload: {
        ...payload,
        status   : true,
        isDialog : true
    }
});

export const closeModal = () => ({
    type: actionTypes.CLOSE_MODAL,
    payload: { status : false }
});