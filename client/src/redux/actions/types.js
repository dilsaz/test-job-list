export const UPDATE_PRIORITY_TYPES  = "UPDATE_PRIORITY_TYPES";
export const LOAD_PRIORITY_TYPES    = "LOAD_PRIORITY_TYPES";
export const CHANGE_LOAD_STATUS     = "CHANGE_LOAD_STATUS";
export const ADD_JOB                = "ADD_JOB";
export const EDIT_JOB               = "EDIT_JOB";
export const REMOVE_JOB             = "REMOVE_JOB";
export const OPEN_MODAL             = "OPEN_MODAL";
export const CLOSE_MODAL            = "CLOSE_MODAL";
