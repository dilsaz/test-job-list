import { createReducer }    from 'redux-immutablejs'
import Immutable            from 'immutable';
import * as actionTypes     from "@actionTypes";

const initialState = {
    priorityTypes   : {},
    jobs            : []
}

const initialData   = Immutable.fromJS( initialState );

export default createReducer(initialData, {

        [actionTypes.UPDATE_PRIORITY_TYPES] : (state, action) => {
                return state.merge({ priorityTypes : action.payload })
        },

        [actionTypes.ADD_JOB] : (state, action) => {
                return  state.updateIn( ['jobs'], arr => arr.push(action.payload) )
        },

        [actionTypes.REMOVE_JOB] : (state, action) => {
                return state.updateIn(['jobs'], arr => arr.slice(0,action.payload).concat( arr.slice(action.payload+1) ) )
        },
               
        [actionTypes.EDIT_JOB] : (state, action) => {
                state = state.toJS();
                state.jobs[action.payload.index] = action.payload.data;
                return Immutable.fromJS(state);
        },
});