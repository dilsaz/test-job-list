import React                from "react"
import { createReducer }    from 'redux-immutablejs'
import Immutable            from 'immutable';
import * as actionTypes     from "@actionTypes";

const initialState = {
    load   : false,
    modal  : {
                status   : false,
                onClose  : () => {},
                isDialog : false,
                title    : "",
                body     : <div></div>
            }
};

const initialData   = Immutable.fromJS( initialState );

export default createReducer(initialData, {

    [actionTypes.CHANGE_LOAD_STATUS] : (state, action) =>
            state.merge({ load : action.payload }),

    [actionTypes.OPEN_MODAL] : (state, action) =>
            state.merge({ modal : action.payload }),

    [actionTypes.CLOSE_MODAL] : (state, action) =>
            state.merge({ modal : action.payload })

});