import { combineReducers }  from 'redux-immutablejs';
import jobData              from "./jobData";
import globalData           from "./globalData";

export default combineReducers({
    jobData : jobData,
    globalData : globalData
});