
import api                              from '@api';
import { put, takeLatest, all, call }   from 'redux-saga/effects';
import * as actionTypes                 from "@actionTypes";

class SagaAsyncInterface{
    constructor(){
        this.name = 'SagaAsyncInterface';
    }

    * getPriorityTypes() {
        try{
            const response = yield call(
                api.fetch, {
                    url: '/api/priority-types',
                    method: 'GET'
                }
            );
            if (response.data)
                yield put({ type: actionTypes.UPDATE_PRIORITY_TYPES, payload: response.data });

        }catch(err){
            console.warn("getPriorityTypes-err :: ",err);
        }
    }
}

const SagaInterface = new SagaAsyncInterface();
export default function* RootSagaAsyncInterface() {
    yield all([
        takeLatest( actionTypes.LOAD_PRIORITY_TYPES,  SagaInterface.getPriorityTypes ),
    ])
}