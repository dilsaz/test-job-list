FROM node:latest

WORKDIR /

COPY ./ ./app

RUN cd ./app/client \
    && yarn \
    && yarn test \
    && yarn build \
    && cd ../ \
    && yarn

EXPOSE 8080
CMD ["node", "./app/index.js"]